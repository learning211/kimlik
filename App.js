import React, {useState} from 'react';

import {
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
} from 'react-native';

const App = () => {
  const [arkaplanRengi, setArkaplanRengi] = useState('white');
  const [tckn] = useState();
  const [ad] = useState();
  const [soyad] = useState();
  const [dogumGun] = useState();
  const [dogumAy] = useState();
  const [dogumYil] = useState();

  const onDogrula = async () => {
    setArkaplanRengi('yellow');

    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/todos/1');
      const json = await response.json();
      setArkaplanRengi('green');
      console.log(json);
    } catch {
      setArkaplanRengi('red');
      console.log('Hata oldu');
    }
  };

  return (
    <SafeAreaView>
      <View style={{backgroundColor: arkaplanRengi}}>
        <Text>TC Kimlik Sorgulama</Text>
        <TextInput style={styles.girdi} value={tckn} placeholder="TCKN" />
        <TextInput style={styles.girdi} value={ad} placeholder="Adınız" />
        <TextInput style={styles.girdi} value={soyad} placeholder="Soyadınız" />
        <TextInput
          style={styles.girdi}
          value={dogumGun}
          placeholder="Doğum gününüz"
        />
        <TextInput
          style={styles.girdi}
          value={dogumAy}
          placeholder="Doğum Ayı"
        />
        <TextInput
          style={styles.girdi}
          value={dogumYil}
          placeholder="Doğum Yılınız"
        />
        <Button onPress={onDogrula} title="Doğrula" />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  girdi: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
  },
});

export default App;
